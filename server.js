require('dotenv').config();
const cors = require('cors');
var express = require('express');
var app = express();
var port = process.env.PORT || 3000;

const requestJSON = require('request-json');

const bodyParser = require('body-parser');
const URL_BASE = "/apiperu/v1/";
const apikeyMLab = 'apiKey=' + process.env.MLABAPIKEY;
const mLabURLbase = 'https://api.mlab.com/api/1/databases/techu18db/collections/';



app.use(bodyParser.json())
app.use(cors());
app.options('*' ,cors());

//Servidor escuchara en la URL (servidor local)
app.listen(port, function() {
  console.log("Node JS escuchando...");
});

// Petición GET usuario id con mLab
app.get(URL_BASE + 'usuario/:id',
function (req, res) {
  console.log(req.params.id);
  var id = req.params.id;
  var queryString = 'q={"usuarioID":' + id + '}&';
  console.log("String");
  console.log(queryString);
  var queryStrField = 'f={"_id":0}&';
  console.log("StrField");
  console.log(queryStrField);
  var httpClient = requestJSON.createClient(mLabURLbase);
  httpClient.get('usuario?' + queryString + queryStrField + apikeyMLab,
    function(err, respuestaMLab, body){
      //console.log("Respuesta mLab correcta.");
      //console.log(respuestaMLab);
      console.log("Body");
      console.log(body);
      // console.log("Response");
      // console.log(response);
      console.log("err");
      console.log(err);
    //  var respuesta = body[0];
      var response = {};
      if(err) {
          response = {"msg" : "Error obteniendo usuario."}
          res.status(500);
      } else {
        if(body.length > 0) {
          response = body;
        } else {
          response = {"msg" : "Usuario no encontrado."}
          res.status(404);
        }
      }
      res.send(response);
    });
});


// GET Relacion de Solicitudes de usuario ya logeado
app.get(URL_BASE + 'solicitud/:id',
function (req, res) {
    console.log(req.params.id);
    var id = req.params.id;
    
    var queryString = 'q={"usuarioID":' + id + '}&';
    var queryStrField = 'f={"_id":0}&';
    var httpClient = requestJSON.createClient(mLabURLbase);
    httpClient.get('solicitud?' + queryString + queryStrField + apikeyMLab,
      function(err, respuestaMLab, body){
        console.log("Body");
        console.log(body);
        console.log("err");
        console.log(err);
        console.log("body.leght");
        console.log(body.length);
        var response = {};
        if(err) {
            response = {"msg" : "Error obteniendo solicitudes de usuario."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Solicitudes de usuario no encontradas."}
            res.status(404);
          }
        }
        res.send(response);
    });
 });



// ANULAR SOLICITUD Anular Solicitud de usuario ya logeado con (DELETE/PUT)
app.delete(URL_BASE + 'anularsolicitud/:id',
function (req, res) {
    console.log(req.params.id);
    var id = req.params.id;
  
    //var queryString = 'q={"nroSolicitud":' + id + '}&';
    //var queryStrField = 'f={"_id":0}&';
    var httpClient = requestJSON.createClient(mLabURLbase);
    let estado = '{"$set":{"estSolicitud":"Anulado"}}';
    
    httpClient.put('solicitud?q={"nroSolicitud": ' + id + '}&' + apikeyMLab, JSON.parse(estado),
     function(err, respuestaMLab, body){
      //console.log("RespuestaMLAB");
      //console.log(respuestaMLab);
          
        console.log("Body");
        console.log(body);
        console.log("err");
        console.log(err);
        console.log("body.lenght");
        console.log(body.length);
        var response = {};
        if(err) {
            response = {"msg" : "Error anulando solicitud."}
            res.status(500);
        } else {
          var n = body;
          console.log(n);
          if(n =  "n: 1"  ) {
            console.log("Entro a body n= 1");
            response = {"msg" : "Solicitud eliminada."}
            res.send(response);
            } else {
            console.log(body.length);
            console.log("Entro a body n<> 1");
            response = {"msg" : "Solicitud no eliminada."}
            res.send(response);
          
          }
        }
    });  
});


// DELETE: Eliminar Solicitud de usuario ya logeado con (DELETE/DELETE)
app.delete(URL_BASE + 'eliminarsolicitud/:id',
  function (req, res) {
   console.log(req.params.id);
   var id = req.params.id;
      var queryStringget = 'q={"nroSolicitud":' + id + '}&';
   //var queryStrField = 'f={"_id":0}&';
   var httpClient = requestJSON.createClient(mLabURLbase);
   httpClient.get('solicitud?' + queryStringget + apikeyMLab,
   function(error, respuestaMLab, body){
    var respuesta = body[0]
    console.log("Body[0]");
    console.log(respuesta);
    // console.log(respuesta);
    // console.log("body.lenght");
    // console.log(body.length);
    var response = {};
    console.log("body.lenght");
    console.log(body.length);
        if(body.length>0) {
      var httpClient = requestJSON.createClient(mLabURLbase);
      httpClient.delete(mLabURLbase + "solicitud/" + respuesta._id.$oid  +'?'+ apikeyMLab,      
      function(err2, respuestaMLab2, body2){
         console.log("Body2");
         console.log(body2);
         console.log("err2");
         console.log(err2);
         console.log("body2.leght");
         console.log(body2.length);
        
        if(err2) {
          //console.log("entro a err2");
          response = {"msg" : "Error eliminado solicitud."}
          res.status(500);
          res.send(response);
        }else {
             //response = body;
             console.log("entro a err2 <>0");
             response = {"msg" : "Solicitud eliminada."}
             res.send(response);
             //res.send({'msg':'Solicitud eliminada'});
          // if(body2.length > 0) {
          //   response = body2;
          //   console.log("entro a body2.lenght>0");
          //   res.send({'msg':'Solicitud eliminada'});
          // } else {
          //   console.log("entro a body2.lenght<>0");
          //   response = {"msg" : "Solicitud no eliminada."}
          //   res.status(404);
          // }
        }
        
  
      });

    }else{
      console.log("entro a body.lenght<>0");
      console.log("entro a body.lenght<>0");
      response = {"msg" : "Solicitud no eliminada."}
      res.status(404);
      res.send(response);
    }
    
  });
});

// AGREGAR: Agregar Solicitud de usuario ya logeado con 
app.post(URL_BASE + 'agregarsolicitud',
  function (req, res) {
   var queryString = 's={"nroSolicitud": -1}&';
   var httpClient = requestJSON.createClient(mLabURLbase);
   httpClient.get('solicitud?' + queryString +  apikeyMLab,
    function(err, _respuestaMLab, body){
     //nuevoID = body.length + 1;
     nuevoID = body[0].nroSolicitud+1;
     console.log("nuevoID");
     console.log(nuevoID);
     console.log("body.lenght")
     console.log(body.length);
     console.log("body(0)")
     console.log(body[0]);
     var nuevaSolicitud = {
       "nroSolicitud" : nuevoID,
       "monSolicitud" : req.body.monSolicitud,
       "montoSolicitud" : req.body.montoSolicitud,
       "estSolicitud" : req.body.estSolicitud,
       "usuarioID" : req.body.usuarioID
     };
     console.log("previa post");
     httpClient.post(mLabURLbase+'solicitud?' +  apikeyMLab, nuevaSolicitud,
      function(errpost, _respuestaMLabpost, bodypost){
        console.log(errpost);
        res.send(bodypost);
    });
  });
});

// MODIFICAR: Modificar Solicitud de usuario ya logeado con 
app.put(URL_BASE + 'modificarsolicitud/:id',
function (req, res) {
    console.log(req.params.id);
    var id = 0;
    id = req.params.id*1;
  
    //var queryString = 'q={"nroSolicitud":' + id + '}&';
    //var queryStrField = 'f={"_id":0}&';
    var httpClient = requestJSON.createClient(mLabURLbase);
    //let estado = '{"$set":{"estSolicitud":"Anulado"}}';
    let updateRecord = {
      "nroSolicitud" : id,
      "monSolicitud" : req.body.monSolicitud,
      "montoSolicitud" : req.body.montoSolicitud,
      "estSolicitud" : req.body.estSolicitud,
      "usuarioID" : req.body.usuarioID
    };

    httpClient.put('solicitud?q={"nroSolicitud": ' + id + '}&' + apikeyMLab, updateRecord,
     function(err, respuestaMLab, body){
      // //console.log("RespuestaMLAB");
      // //console.log(respuestaMLab);
          
      //   console.log("Body");
      //   console.log(body);
      //   console.log("err");
      //   console.log(err);
      //   console.log("body.lenght");
      //   console.log(body.length);
        var response = {};
        if(err) {
            response = {"msg" : "Error modificando solicitud."}
            res.status(500);
        } else {
          var n = body;
          console.log(n);
          if(n =  "n: 1"  ) {
            console.log("Entro a body n= 1");
            response = {"msg" : "Solicitud modificada."}
            res.send(response);
            } else {
            console.log(body.length);
            console.log("Entro a body n<> 1");
            response = {"msg" : "Solicitud no modificada."}
            res.send(response);
          
          }
        }
    });  
});



// LOGIN: POST Login usuario con mLab
app.post(URL_BASE + 'loginbd',
  function (req, res) {
    console.log(req.body.password);
    var httpClient = requestJSON.createClient(mLabURLbase);
    var email = req.body.email;
    var pass = req.body.password;
    var queryString = `q={"email":"${email}","password":"${pass}"}&`;
    console.log(queryString);
    var limFilter = 'l=1&';
    
    httpClient.get('usuario?' + queryString + limFilter + apikeyMLab,
    function(err, respuestaMLab, body) {
      console.log("Body");
      console.log(body);
      console.log("err");
      console.log(err);
    
      if(!err) {
        if (body.length == 1) { // Existe un usuario que cumple 'queryString'
          let login = '{"$set":{"logged":true}}';
          console.log("login");
          console.log(login);
          console.log("Body(0).usuarioID");
          console.log(body[0].usuarioID);
          console.log("Bodylenght");
          console.log(body.length);

          httpClient.put('usuario?q={"usuarioID": ' + body[0].usuarioID + '}&' + apikeyMLab, JSON.parse(login),
            function(errPut, resPut, bodyPut) {
              console.log("Bodyput");
              console.log(bodyPut);
              console.log("errput");
              console.log(errPut);
              console.log("Body(0).usuarioID");
              console.log(body[0].usuarioID);
              res.send({'msg':'Login correcto', 'email':body[0].email, 'usuarioID':body[0].usuarioID, 'name':body[0].name,'logged':body[0].logged});
              });
      }
      else {
        res.status(404).send({"msg":"Usuario/password no válido."});
      }
    } else {
      res.status(500).send({"msg": "Error en petición a mLab."});
    }
});
});

//LOGOUT: POST Logout usuario ya logueado
app.post(URL_BASE + "logoutbd",
  function(req, res) {
    var email = req.body.email;  
    var queryString = `q={"email":"${email}"}&`;
    var httpClient = requestJSON.createClient(mLabURLbase);
    httpClient.get(`usuario?${queryString}${apikeyMLab}`,
      function(error, respuestaMLab, body) {
        var respuesta = body[0]; // Asegurar único usuario
        console.log("respuesta");
        console.log(respuesta);
        console.log("error");
        console.log(error);
        if(!error) {
          if (respuesta != undefined) { // Existe un usuario que cumple 'queryString'
            let logout = '{"$unset":{"logged":true}}';
            httpClient.put('usuario?q={"usuarioID": ' + respuesta.usuarioID + '}&' + apikeyMLab, JSON.parse(logout),
            //clienteMlab.put('user/' + respuesta._id.$oid + '?' + apikeyMLab, JSON.parse(logout),
              function(errPut, resPut, bodyPut) {
                res.send({'msg':'Logout correcto', 'user':respuesta.email});
                // If bodyPut.n == 1, put de mLab correcto
              });
            } else {
                res.status(404).send({"msg":"Logout fallido!"});
            }
        }else {
          res.status(500).send({"msg": "Error en petición a mLab."});
        }
    });
});
